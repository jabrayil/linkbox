<?php

namespace Common\Base\Helpers;

final class GenDigitCode
{
    public static function generate(?int $codeLength = 4) : string {
        while (true) {
            $code = substr(str_shuffle("0123456789"), 0, $codeLength);

            if (
                strlen($code) === $codeLength
                &&
                $code[0] != 0
            ) {
                break;
            }
        }

        return $code;
    }
}
