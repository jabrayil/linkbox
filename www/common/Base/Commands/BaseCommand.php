<?php

namespace Common\Base\Commands;

interface BaseCommand
{
    public function execute() : bool;
}
