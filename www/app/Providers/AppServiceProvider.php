<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        $rootPath = dirname(__FILE__, 3);

        $this->loadMigrationsFrom([
            $rootPath . '/backend/User/Migrations',
            $rootPath . '/backend/Auth/Migrations',
        ]);
    }
}
