<?php

use Backend\Auth\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/constructor', function () {
    return 'constructor';
});

Route::middleware(['userAuth'])->group(function () {
    Route::get('/profile', function () {

        $email = session('user')->email;
        echo "<h1>UserEmail: $email</h1><br>";

        echo "<a href='/logout'>Выйти</a><br>";
    });
});

Route::middleware(['userNotAuth'])->group(function () {
    Route::get('/create', [AuthController::class, 'createFront']);

    Route::post('/create', [AuthController::class, 'create'])->name(AuthController::CREATE);

    Route::get('/checkCode', [AuthController::class, 'checkCodeFront']);

    Route::post('/checkCode', [AuthController::class, 'checkCode'])->name(AuthController::CHECK_CODE);

    Route::get('/authWithPassword', [AuthController::class, 'authWithPasswordFront']);

    Route::post('/authWithPassword', [AuthController::class, 'authWithPassword'])->name(AuthController::AUTH_WITH_PASSWORD);

    Route::get('/forgetPassword', [AuthController::class, 'forgetPassword']);

    Route::get('/changeUser', [AuthController::class, 'changeUser']);

    Route::get('/sendCodeAgain', [AuthController::class, 'sendCodeAgain']);

    Route::get('/setPassword', [AuthController::class, 'setPasswordFront']);

    Route::post('/setPassword', [AuthController::class, 'setPassword'])->name(AuthController::SET_PASSWORD);
});

Route::middleware(['userAuth'])->group(function () {
    Route::get('/logout', [AuthController::class, 'logout']);
});
