<?php

namespace Backend\Auth\DataTransferObjects;

use Backend\Auth\ValueObjects\Code;
use Backend\Auth\ValueObjects\EmailVo;

final class UserCheckCodeDto
{
    private EmailVo $email;

    private Code $code;

    /**
     * UserCheckCodeDto constructor.
     * @param EmailVo $email
     * @param Code $code
     */
    public function __construct(
        EmailVo $email,
        Code $code
    )
    {
        $this->email = $email;
        $this->code = $code;
    }

    /**
     * @return Code
     */
    public function getCode(): Code
    {
        return $this->code;
    }

    /**
     * @return EmailVo
     */
    public function getEmail(): EmailVo
    {
        return $this->email;
    }

}
