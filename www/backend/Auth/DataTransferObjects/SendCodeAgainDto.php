<?php

namespace Backend\Auth\DataTransferObjects;

use Backend\Auth\ValueObjects\EmailVo;

final class SendCodeAgainDto
{
    private EmailVo $email;

    public function __construct(EmailVo $email)
    {
        $this->email = $email;
    }

    /**
     * @return EmailVo
     */
    public function getEmail(): EmailVo
    {
        return $this->email;
    }

}
