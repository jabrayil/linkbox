<?php

namespace Backend\Auth\DataTransferObjects;

use Backend\Auth\ValueObjects\EmailVo;
use Backend\Auth\ValueObjects\PasswordVo;

final class UserAuthWithPasswordDto
{
    private EmailVo $email;
    private PasswordVo $password;

    public function __construct(
        EmailVo $email,
        PasswordVo $password
    )
    {
        $this->email = $email;
        $this->password = $password;
    }

    /**
     * @return EmailVo
     */
    public function getEmail(): EmailVo
    {
        return $this->email;
    }

    /**
     * @return PasswordVo
     */
    public function getPassword(): PasswordVo
    {
        return $this->password;
    }
}
