<?php

namespace Backend\Auth\DataTransferObjects;

use Backend\Auth\ValueObjects\EmailVo;
use Backend\Auth\ValueObjects\PasswordVo;

final class UserSetPasswordDto
{
    private EmailVo $email;
    private PasswordVo $password;
    private PasswordVo $passwordDuplicate;

    public function __construct(
        EmailVo $email,
        PasswordVo $password,
        PasswordVo $passwordDuplicate
    )
    {
        $this->email = $email;
        $this->password = $password;
        $this->passwordDuplicate = $passwordDuplicate;
    }

    /**
     * @return EmailVo
     */
    public function getEmail(): EmailVo
    {
        return $this->email;
    }

    /**
     * @return PasswordVo
     */
    public function getPassword(): PasswordVo
    {
        return $this->password;
    }

    /**
     * @return PasswordVo
     */
    public function getPasswordDuplicate(): PasswordVo
    {
        return $this->passwordDuplicate;
    }

}
