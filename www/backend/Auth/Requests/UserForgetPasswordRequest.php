<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserForgetPasswordDto;
use Backend\Auth\ValueObjects\EmailVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserForgetPasswordRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function getDto() : UserForgetPasswordDto {
        return new UserForgetPasswordDto(
            EmailVo::create($this->input('email'))
        );
    }

}
