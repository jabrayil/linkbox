<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserCheckCodeDto;
use Backend\Auth\ValueObjects\Code;
use Backend\Auth\ValueObjects\EmailVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserCheckCodeRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
            'code' => 'required|int'
        ];
    }

    public function getDto() : UserCheckCodeDto {
        return new UserCheckCodeDto(
            EmailVo::create($this->input('email')),
            Code::create($this->input('code'))
        );
    }

}
