<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserCreateDto;
use Backend\Auth\ValueObjects\EmailVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserCreateRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function getDto() : UserCreateDto {
        return new UserCreateDto(
            EmailVo::create($this->input('email'))
        );
    }

}
