<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserAuthWithPasswordDto;
use Backend\Auth\ValueObjects\EmailVo;
use Backend\Auth\ValueObjects\PasswordVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserAuthWithPasswordRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string'
        ];
    }

    public function getDto() : UserAuthWithPasswordDto {
        return new UserAuthWithPasswordDto(
            EmailVo::create($this->input('email')),
            PasswordVo::create($this->input('password'))
        );
    }

}
