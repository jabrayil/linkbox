<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserSetPasswordDto;
use Backend\Auth\ValueObjects\EmailVo;
use Backend\Auth\ValueObjects\PasswordVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserSetPasswordRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function getDto() : UserSetPasswordDto {
        return new UserSetPasswordDto(
            EmailVo::create($this->input('email')),
            PasswordVo::create($this->input('password')),
            PasswordVo::create($this->input('password_duplicate')),
        );
    }

}
