<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\SendCodeAgainDto;
use Backend\Auth\ValueObjects\EmailVo;
use Illuminate\Foundation\Http\FormRequest;

final class SendCodeAgainRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function getDto() : SendCodeAgainDto {
        return new SendCodeAgainDto(
            EmailVo::create($this->input('email'))
        );
    }

}
