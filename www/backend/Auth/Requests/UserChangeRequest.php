<?php

namespace Backend\Auth\Requests;


use Backend\Auth\DataTransferObjects\UserChangeDto;
use Backend\Auth\ValueObjects\EmailVo;
use Illuminate\Foundation\Http\FormRequest;

final class UserChangeRequest extends FormRequest {

    public function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    public function getDto() : UserChangeDto {
        return new UserChangeDto(
            EmailVo::create($this->input('email'))
        );
    }

}
