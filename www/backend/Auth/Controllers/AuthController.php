<?php

namespace Backend\Auth\Controllers;

use App\Http\Controllers\Controller;
use Backend\Auth\Commands\SendCodeAgainCommand;
use Backend\Auth\Commands\UserAuthWithPasswordCommand;
use Backend\Auth\Commands\UserChangeCommand;
use Backend\Auth\Commands\UserCheckCodeCommand;
use Backend\Auth\Commands\UserCreateCommand;
use Backend\Auth\Commands\UserForgetPasswordCommand;
use Backend\Auth\Commands\UserSetPasswordCommand;
use Backend\Auth\Exceptions\AuthCodeNotFound;
use Backend\Auth\Exceptions\EmailNotEqual;
use Backend\Auth\Exceptions\PasswordsNotEqual;
use Backend\Auth\Requests\SendCodeAgainRequest;
use Backend\Auth\Requests\UserAuthWithPasswordRequest;
use Backend\Auth\Requests\UserChangeRequest;
use Backend\Auth\Requests\UserCheckCodeRequest;
use Backend\Auth\Requests\UserCreateRequest;
use Backend\Auth\Requests\UserForgetPasswordRequest;
use Backend\Auth\Requests\UserSetPasswordRequest;
use Backend\User\Exceptions\InvalidUserCredentials;
use Backend\User\Exceptions\UserAlreadyExists;
use Backend\User\Exceptions\UserNotFound;
use Illuminate\Http\Request;
use InvalidArgumentException;

class AuthController extends Controller {

    const CREATE = 'create';
    const AUTH_WITH_PASSWORD = 'authWithPassword';
    const FORGET_PASSWORD = 'forgetPassword';
    const CHECK_CODE = 'checkCode';
    const SET_PASSWORD = 'setPassword';
    const SEND_CODE_AGAIN = 'sendCodeAgain';
    const CHANGE_USER = 'changeUser';

    public function createFront() {
        return view(self::CREATE);
    }

    public function create(UserCreateRequest $userCreateRequest) {
        try {
            $command = UserCreateCommand::create(
                $userCreateRequest->getDto()
            );
        } catch (InvalidArgumentException $invalidArgumentException) {

            return back()->withErrors(["email" => $invalidArgumentException->getMessage()]);
        }

        try {
            if ($command->execute()) {
                session(["email" => $userCreateRequest->getDto()->getEmail()->value()]);

                return redirect()->route(self::CHECK_CODE);
            }
        } catch (UserAlreadyExists $userAlreadyExists) {

            session(["email" => $userCreateRequest->getDto()->getEmail()->value()]);

            return redirect()
                ->route(self::AUTH_WITH_PASSWORD);
        }

        return redirect()->route(self::CREATE);
    }

    public function authWithPasswordFront(Request $request) {
        if (
            $this->_checkPrevious([
                self::CREATE,
                self::AUTH_WITH_PASSWORD
            ])
        ) {
            $email = $request->session()->get("email");

            if (!$email) {
                return redirect()
                    ->route(self::CREATE);
            }

            return view(self::AUTH_WITH_PASSWORD, [
                "email" => $email
            ]);
        }

        return redirect()
            ->route(self::CREATE);
    }

    public function authWithPassword(UserAuthWithPasswordRequest $userAuthWithPasswordRequest) {
        try {
            $command = UserAuthWithPasswordCommand::create(
                $userAuthWithPasswordRequest->getDto()
            );
        } catch (InvalidArgumentException $invalidArgumentException) {

            return back()->withErrors(["password" => $invalidArgumentException->getMessage()]);
        }

        try {
            if ($command->execute()) {

                return redirect('/profile');
            }
        } catch (InvalidUserCredentials $invalidUserCredentials) {

            return back()->withErrors(["password" => "Invalid email or password"]);
        }

        return redirect()->route(self::CREATE);
    }

    public function forgetPassword(UserForgetPasswordRequest $userForgetPasswordRequest) {
        if (
            $this->_checkPrevious([
                self::AUTH_WITH_PASSWORD,
                self::FORGET_PASSWORD
            ])
        ) {
            try {
                $command = UserForgetPasswordCommand::create(
                    $userForgetPasswordRequest->getDto()
                );
            } catch (InvalidArgumentException $invalidArgumentException) {

                return back()->withErrors(["email" => $invalidArgumentException->getMessage()]);
            }

            try {
                if ($command->execute()) {

                    return redirect()->route(self::CHECK_CODE);
                }
            } catch (UserNotFound $userNotFound) {

                return back()->withErrors([
                    "user" => 'User with email ' . $userForgetPasswordRequest->getDto()->getEmail()->value() . ' not found'
                ]);
            }

            return redirect()->route(self::CHECK_CODE);
        }

        return redirect()
            ->route(self::CREATE);
    }

    public function changeUser(UserChangeRequest $userChangeRequest) {
        if (
            $this->_checkPrevious([
                self::AUTH_WITH_PASSWORD,
                self::CHANGE_USER
            ])
        ) {
            try {
                $command = UserChangeCommand::create(
                    $userChangeRequest->getDto()
                );
            } catch (InvalidArgumentException $invalidArgumentException) {

                return back()->withErrors(["email" => $invalidArgumentException->getMessage()]);
            }

            if ($command->execute()) {

                return redirect()->route(self::CREATE);
            }
        }

        return redirect()
            ->route(self::CREATE);
    }

    public function logout() {
        session()->remove("user");

        return redirect('/');
    }

    public function checkCodeFront(Request $request) {
        if (
            $this->_checkPrevious([
                self::CREATE,
                self::FORGET_PASSWORD,
                self::CHECK_CODE,
                self::SEND_CODE_AGAIN
            ])
        ) {
            $email = $request->session()->get("email");

            if (!$email) {
                return redirect()
                    ->route(self::CREATE);
            }

            return view(self::CHECK_CODE, [
                "email" => $email
            ]);
        }

        return redirect()
            ->route(self::CREATE);
    }

    public function checkCode(UserCheckCodeRequest $userCheckCodeRequest) {
        try {
            $command = UserCheckCodeCommand::create(
                $userCheckCodeRequest->getDto()
            );
        } catch (InvalidArgumentException $invalidArgumentException) {

            return back()->withErrors(["code" => $invalidArgumentException->getMessage()]);
        }

        try {
            if ($command->execute()) {

                return redirect()->route(self::SET_PASSWORD);
            }
        } catch (UserNotFound $userNotFound) {

            return back()->withErrors([
                "user" => 'User with email ' . $userCheckCodeRequest->getDto()->getEmail()->value() . ' not found'
            ]);
        } catch (AuthCodeNotFound $authCodeNotFound) {

            return back()->withErrors([
                "user" => 'Auth code ' . $userCheckCodeRequest->getDto()->getCode()->value() . ' incorrect'
            ]);
        }

        return redirect()->route(self::CREATE);
    }

    public function setPasswordFront(Request $request) {
        if (
            $this->_checkPrevious([
                self::CHECK_CODE,
                self::SET_PASSWORD
            ])
        ) {
            $email = $request->session()->get("email");

            if (!$email) {
                return redirect()
                    ->route(self::CREATE);
            }

            return view(self::SET_PASSWORD, [
                "email" => $email
            ]);
        }

        return redirect()
            ->route(self::CREATE);
    }

    public function setPassword(UserSetPasswordRequest $userSetPasswordRequest) {
        try {
            $command = UserSetPasswordCommand::create(
                $userSetPasswordRequest->getDto()
            );
        } catch (InvalidArgumentException $invalidArgumentException) {

            return back()->withErrors(["default" => $invalidArgumentException->getMessage()]);
        }

        try {
            if ($command->execute()) {

                return redirect('/profile');
            }
        } catch (PasswordsNotEqual $passwordsNotEqual) {

            return back()->withErrors([
                "password" => 'Entered passwords must be the same'
            ]);
        } catch (EmailNotEqual $emailNotEqual) {
            return back()->withErrors([
                "email" => 'Incorrect email'
            ]);
        }

        return redirect()->route(self::CREATE);
    }

    public function sendCodeAgain(SendCodeAgainRequest $sendCodeAgainRequest) {
        if (
            $this->_checkPrevious([
                self::CHECK_CODE,
                self::SEND_CODE_AGAIN
            ])
        ) {
            try {
                $command = SendCodeAgainCommand::create(
                    $sendCodeAgainRequest->getDto()
                );
            } catch (InvalidArgumentException $invalidArgumentException) {

                return back()->withErrors(["email" => $invalidArgumentException->getMessage()]);
            }

            try {
                if ($command->execute()) {

                    session(["email" => $sendCodeAgainRequest->getDto()->getEmail()->value()]);

                    return redirect()->route(self::CHECK_CODE);
                }
            } catch (UserNotFound $userNotFound) {

                return back()->withErrors([
                    "user" => 'User with email ' . $sendCodeAgainRequest->getDto()->getEmail()->value() . ' not found'
                ]);
            }
        }

        return redirect()->route(self::CREATE);
    }

    protected function _checkPrevious(array $allowedUrls) : bool {
        $previousUrl = session('_previous');

        foreach ($allowedUrls as $allowedUrl) {
            if ($previousUrl && str_contains($previousUrl["url"], $allowedUrl)) {
                return true;
            }
        }

        return false;
    }
}
