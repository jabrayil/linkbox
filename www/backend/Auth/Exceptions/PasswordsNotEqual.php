<?php


namespace Backend\Auth\Exceptions;


use RuntimeException;

final class PasswordsNotEqual extends RuntimeException
{

}
