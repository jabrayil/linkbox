<?php

namespace Backend\Auth\ValueObjects;

use InvalidArgumentException;

final class PasswordVo {

    const MINIMUM_PASSWORD_LENGTH = 6;

    /** @var string */
    private $password;

    private function __construct(string $password)
    {
        if (strlen($password) <= self::MINIMUM_PASSWORD_LENGTH - 1) {
            throw new InvalidArgumentException(
                'Password must be at least 6 characters');
        }
        $this->password = $password;
    }

    public static function create(string $password)
    {
        return new static($password);
    }

    public function value(): string
    {
        return $this->password;
    }
}
