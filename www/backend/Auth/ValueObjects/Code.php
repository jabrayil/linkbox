<?php

namespace Backend\Auth\ValueObjects;

use Backend\Auth\Commands\UserCreateCommand;
use InvalidArgumentException;

final class Code {

    /** @var string */
    private $code;

    private function __construct(string $code)
    {
        if ((int)strlen($code) !== UserCreateCommand::DEFAULT_CODE_LENGTH) {
            throw new InvalidArgumentException(
                'Code ' . $code . ' must be 6 characters');
        }
        $this->code = $code;
    }

    public static function create(string $code)
    {
        return new static($code);
    }

    public function value(): string
    {
        return $this->code;
    }
}
