`<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateAuthCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            DB::statement('
                CREATE TABLE auth_codes
                (
                    id bigserial primary key not null,
                    user_id bigint not null,
                    code varchar not null,
                    created_at timestamp,
                    updated_at timestamp,
                    deleted_at timestamp
                );
            ');

            DB::statement('
                ALTER TABLE auth_codes ADD CONSTRAINT auth_codes_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // для изменения
        // создайте новую миграцию
    }
}
