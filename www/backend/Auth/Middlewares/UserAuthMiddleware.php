<?php

namespace Backend\Auth\Middlewares;

use Backend\Auth\Controllers\AuthController;
use Backend\User\Models\User;
use Closure;

final class UserAuthMiddleware {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!(session('user') instanceof User)) {
            return redirect()->route(AuthController::CREATE);
        }

        return $next($request);
    }
}
