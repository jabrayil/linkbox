<?php


namespace Backend\Auth\Models;

use Illuminate\Database\Eloquent\Model;

final class AuthCode extends Model {

    protected $table = 'auth_codes';

}
