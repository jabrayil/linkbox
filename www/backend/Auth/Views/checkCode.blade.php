<form action="/{{ \Backend\Auth\Controllers\AuthController::CHECK_CODE }}" method="POST">
    @csrf

    Здравствуйте, <?= $email ?>

    <input type="hidden" name="email" value="{{ $email }}">

    <br> Введите код отправленный вам на эмайл: <br>
    <input type="text" name="code">

    <input type="submit" value="Отправить">
</form>

<a href="/{{ \Backend\Auth\Controllers\AuthController::SEND_CODE_AGAIN }}?email={{ $email }}">Отправить код заново</a>

@include('errors')
