<form action="/{{ \Backend\Auth\Controllers\AuthController::AUTH_WITH_PASSWORD }}" method="POST">
    @csrf

    Здравствуйте, <?= $email ?>

    Войти по паролю <br>

    <input type="hidden" name="email" value="{{ $email }}">

    <input type="password" name="password">

    <input type="submit" value="Отправить">
</form>

<a href="/{{ \Backend\Auth\Controllers\AuthController::FORGET_PASSWORD }}?email={{ $email }}">Забыл пароль</a>

<a href="/{{ \Backend\Auth\Controllers\AuthController::CHANGE_USER }}?email={{ $email }}">Сменить пользователя</a>

@include('errors')
