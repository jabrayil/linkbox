<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserAuthWithPasswordDto;
use Backend\User\Exceptions\InvalidUserCredentials;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;

final class UserAuthWithPasswordCommand implements BaseCommand
{
    private UserAuthWithPasswordDto $userAuthDto;

    public function __construct(UserAuthWithPasswordDto $userAuthDto)
    {
        $this->userAuthDto = $userAuthDto;
    }

    public static function create(UserAuthWithPasswordDto $userAuthDto) : self
    {
        return new self($userAuthDto);
    }

    public function execute(): bool
    {
        $user = User::where([
            "email" => $this->userAuthDto->getEmail()->value(),
            "password" => $this->userAuthDto->getPassword()->value()
        ])->first();

        if (!($user instanceof User)) {
            throw new InvalidUserCredentials();
        }

        session(["user" => $user]);

        return true;
    }
}
