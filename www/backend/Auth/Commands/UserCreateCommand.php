<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserCreateDto;
use Backend\Auth\Models\AuthCode;
use Backend\User\Exceptions\UserAlreadyExists;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;
use Common\Base\Helpers\GenDigitCode;
use Illuminate\Support\Facades\DB;

final class UserCreateCommand implements BaseCommand
{
    const DEFAULT_CODE_LENGTH = 6;

    private UserCreateDto $userCreateDto;

    /**
     * UserCreateCommand constructor.
     * @param UserCreateDto $userCreateDto
     */
    public function __construct(UserCreateDto $userCreateDto)
    {
        $this->userCreateDto = $userCreateDto;
    }

    public static function create(UserCreateDto $userCreateDto) : self
    {
        return new self($userCreateDto);
    }

    public function execute(): bool
    {
        $code = GenDigitCode::generate(self::DEFAULT_CODE_LENGTH);

        $user = User::where([
            "email" => $this->userCreateDto->getEmail()->value()
        ])->first();

        if ($user instanceof User) {
            throw new UserAlreadyExists();
        }

        DB::transaction(function () use ($code) {
            // create user
            $user = new User();
            $user->email = $this->userCreateDto
                ->getEmail()
                ->value();
            $user->save();

            $authCode = new AuthCode();
            $authCode->user_id = $user->id;
            $authCode->code = $code;
            $authCode->save();
        });

        //TODO send email

        return true;
    }
}
