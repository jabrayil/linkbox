<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserSetPasswordDto;
use Backend\Auth\Exceptions\EmailNotEqual;
use Backend\Auth\Exceptions\PasswordsNotEqual;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;

final class UserSetPasswordCommand implements BaseCommand {

    private UserSetPasswordDto $userSetPasswordDto;

    public function __construct(UserSetPasswordDto $userSetPasswordDto)
    {
        $this->userSetPasswordDto = $userSetPasswordDto;
    }

    public static function create(UserSetPasswordDto $userSetPasswordDto) : self
    {
        return new self($userSetPasswordDto);
    }

    public function execute(): bool
    {
        $password = $this->userSetPasswordDto->getPassword()->value();
        $passwordDuplicate = $this->userSetPasswordDto->getPasswordDuplicate()->value();

        if ($password !== $passwordDuplicate) {
            throw new PasswordsNotEqual();
        }

        $secureEmail = session('email');
        if ($secureEmail !== $this->userSetPasswordDto->getEmail()->value()) {
            throw new EmailNotEqual();
        }

        $user = User::where([
            "email" => $secureEmail
        ])->first();

        $user->password = $password;
        $user->save();

        session(["user" => $user]);

        return true;
    }

}
