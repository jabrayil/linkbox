<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserForgetPasswordDto;
use Backend\Auth\Models\AuthCode;
use Backend\User\Exceptions\UserAlreadyExists;
use Backend\User\Exceptions\UserNotFound;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;
use Common\Base\Helpers\GenDigitCode;
use Illuminate\Support\Facades\DB;

final class UserForgetPasswordCommand implements BaseCommand
{
    const DEFAULT_CODE_LENGTH = 6;

    private UserForgetPasswordDto $userForgetPasswordDto;

    public function __construct(UserForgetPasswordDto $userForgetPasswordDto)
    {
        $this->userForgetPasswordDto = $userForgetPasswordDto;
    }

    public static function create(UserForgetPasswordDto $userForgetPasswordDto) : self
    {
        return new self($userForgetPasswordDto);
    }

    public function execute(): bool
    {
        $code = GenDigitCode::generate(self::DEFAULT_CODE_LENGTH);

        $user = User::where([
            "email" => $this->userForgetPasswordDto->getEmail()->value()
        ])->first();

        if (!($user instanceof User)) {
            throw new UserNotFound();
        }

        $authCode = AuthCode::where([
            "user_id" => $user->id
        ])->first();

        if ($authCode instanceof AuthCode) {
            $authCode->delete();
        }

        $authCode = new AuthCode();
        $authCode->user_id = $user->id;
        $authCode->code = $code;
        $authCode->save();

        //TODO send email

        return true;
    }
}
