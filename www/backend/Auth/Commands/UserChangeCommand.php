<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserChangeDto;
use Common\Base\Commands\BaseCommand;


final class UserChangeCommand implements BaseCommand
{
    private UserChangeDto $userChangeDto;

    public function __construct(UserChangeDto $userChangeDto)
    {
        $this->userChangeDto = $userChangeDto;
    }

    public static function create(UserChangeDto $userChangeDto) : self
    {
        return new self($userChangeDto);
    }

    public function execute(): bool
    {
        session()->remove("email");

        return true;
    }
}
