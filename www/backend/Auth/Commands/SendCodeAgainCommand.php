<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\SendCodeAgainDto;
use Backend\Auth\Models\AuthCode;
use Backend\User\Exceptions\UserNotFound;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;
use Common\Base\Helpers\GenDigitCode;

final class SendCodeAgainCommand implements BaseCommand
{
    const DEFAULT_CODE_LENGTH = 6;

    private SendCodeAgainDto $sendCodeAgainDto;

    public function __construct(SendCodeAgainDto $sendCodeAgainDto)
    {
        $this->sendCodeAgainDto = $sendCodeAgainDto;
    }

    public static function create(SendCodeAgainDto $sendCodeAgainDto) : self
    {
        return new self($sendCodeAgainDto);
    }

    public function execute(): bool
    {
        $code = GenDigitCode::generate(self::DEFAULT_CODE_LENGTH);

        $user = User::where([
            "email" => $this->sendCodeAgainDto->getEmail()->value()
        ])->first();

        if (!($user instanceof User)) {
            throw new UserNotFound();
        }

        $authCode = AuthCode::where([
            "user_id" => $user->id
        ])->first();

        if ($authCode instanceof AuthCode) {
            $authCode->delete();
        }

        $authCode = new AuthCode();
        $authCode->user_id = $user->id;
        $authCode->code = $code;
        $authCode->save();

        //TODO send email

        return true;
    }
}
