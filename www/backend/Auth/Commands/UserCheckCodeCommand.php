<?php


namespace Backend\Auth\Commands;

use Backend\Auth\DataTransferObjects\UserCheckCodeDto;
use Backend\Auth\Exceptions\AuthCodeNotFound;
use Backend\Auth\Models\AuthCode;
use Backend\User\Exceptions\UserNotFound;
use Backend\User\Models\User;
use Common\Base\Commands\BaseCommand;

final class UserCheckCodeCommand implements BaseCommand {

    private UserCheckCodeDto $userCheckCodeDto;

    public function __construct(UserCheckCodeDto $userCheckCodeDto)
    {
        $this->userCheckCodeDto = $userCheckCodeDto;
    }

    public static function create(UserCheckCodeDto $userCheckCodeDto) : self
    {
        return new self($userCheckCodeDto);
    }

    public function execute(): bool
    {
        $user = User::where([
            "email" => $this->userCheckCodeDto->getEmail()->value(),
        ])->first();

        if (!($user instanceof User)) {
            throw new UserNotFound();
        }

        $authCode = AuthCode::where([
            "user_id" => $user->id,
            "code" => $this->userCheckCodeDto->getCode()->value()
        ])->first();

        if (!($authCode instanceof AuthCode)) {
            throw new AuthCodeNotFound();
        }

        $authCode->delete();
        session(["email" => $this->userCheckCodeDto->getEmail()->value()]);

        return true;
    }

}
