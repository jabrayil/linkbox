<?php


namespace Backend\User\Models;

use Illuminate\Database\Eloquent\Model;

final class User extends Model {

    protected $table = 'users';

}
