<?php


namespace Backend\User\Exceptions;


use RuntimeException;

final class UserNotFound extends RuntimeException
{

}
