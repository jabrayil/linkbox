<?php


namespace Backend\User\Exceptions;


use RuntimeException;

final class UserAlreadyExists extends RuntimeException
{

}
