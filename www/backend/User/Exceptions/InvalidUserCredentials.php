<?php


namespace Backend\User\Exceptions;


use RuntimeException;

final class InvalidUserCredentials extends RuntimeException
{

}
