<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            DB::statement('
                CREATE TABLE users
                (
                    id bigserial primary key not null,
                    email varchar,
                    password varchar,
                    email_confirmed boolean,
                    last_login timestamp,
                    created_at timestamp,
                    updated_at timestamp,
                    deleted_at timestamp
                );
            ');

            DB::statement('
                ALTER TABLE users 
                ADD CONSTRAINT unique_email 
                UNIQUE (email);
            ');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // для изменения
        // создайте новую миграцию
    }
}
